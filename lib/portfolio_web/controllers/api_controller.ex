defmodule PortfolioWeb.ApiController do
  use PortfolioWeb, :controller
  use Agent

  alias Portfolio.Server
  alias Portfolio.Email
  alias Portfolio.Mailer


  @doc """
  Retrieve an articles data in the database
  """
  def get_articles(conn, %{"title" => title}) do
    ret = Server.get(title) |> List.first

    json(conn, %{data: ret})
  end

  @doc """
  Retrieve a filtered version of articles for the presentation
  """
  def get_list(conn, _params) do
    ret = Server.get_index |> Enum.shuffle

    json(conn, %{data: ret})
  end

  @doc """
  Retrieve the tag list
  """
  def get_tag(conn, _params) do
    pid = Process.whereis(:tag)

    if pid == nil do
      Agent.start_link(fn -> Server.get_tag end, name: :tag)
    end

    ret = Agent.get(:tag, fn state -> state end)

    json(conn, %{data: ret})
  end

  @doc """
  send a mail
  """
  def contact_email(conn, %{"email" => email, "message" => message}) do
    Email.contact_email(email, message) |> Mailer.deliver_now()

    json(conn, %{mail: "OK"})
  end
end
