defmodule Portfolio.Server do
  @moduledoc """
  This GenServer module will load data from a CSV file and act like a really simple SQL database.
  """
  use GenServer

  #================================================
  #------------------------------------------------
  # API

  @doc """
  Initialing the Gen server by calling some private function: init_ets_table/0, seed_ets_table/0
  """

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  @spec get(String.t) :: map
  @doc """
  get/1 will take a name in english of an articles and return it.
  """

  def get(title) do
    GenServer.call(__MODULE__, {:get, title})
  end

  @spec get_index :: list(map)
  @doc """
  get_index/0, will return a filtered version of all articles for the index page
  """

  def get_index do
    GenServer.call(__MODULE__, :get_index)
  end

  @spec get_tag :: list(String.t)
  @doc """
  get_tag/0, will return a list of all tag present in the database
  """

  def get_tag do
    GenServer.call(__MODULE__, :get_tag)
  end

  #================================================
  #------------------------------------------------
  # Callbacks


  def init(_) do
    init_ets_table()
    seed_ets_table()

    {:ok, :success}
  end

  def handle_call({:get, title}, _from, state) do
    data = :ets.select(:articles, [{
      {:_, :"$1", :"$2"},
      [{:==, :"$1", {:const, title}}],
      [:"$2"]
      }])

    {:reply, data, state}
  end

  def handle_call(:get_index, _from, state) do
    index = :ets.tab2list(:articles)
          |> Enum.map(fn {id, _, el} ->
            %{
              id: id,
              title_en: el[:title_en],
              title_fr: el[:title_fr],
              thumb: el[:thumb],
              tag: el[:tag]
            }
          end)

    {:reply, index, state}
  end

  def handle_call(:get_tag, _from, state) do
    tag = :ets.tab2list(:articles)
           |> Enum.map(fn {_, _, element} -> element[:tag] end)
           |> Enum.uniq
           |> Enum.sort

    {:reply, tag, state}
  end


  # INIT AND SEED

  defp init_ets_table do
    :ets.new(:articles, [:named_table, :public, :bag])
  end

  defp seed_ets_table do
    "priv/data.csv"
    |> File.stream!
    |> Stream.drop(1)
    |> CSV.decode(headers: [:title_en, :title_fr, :thumb, :tag, :txt_en, :txt_fr])
    |> Enum.with_index
    |> Enum.each(&match_ets_store/1)
  end

  defp match_ets_store(row) do
    {value, id} = row
    {_, data} = value

    :ets.insert(:articles, {id, data[:title_en], data})
  end

end

