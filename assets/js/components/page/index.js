import React from 'react';


import Header from "./index/header.js"
import Articles from "./index/articles.js"
import Separator from "./index/separator.js"
import About from "./index/about.js"
import Contact from "./index/contact.js"
import Footer from "./footer.js"

const Index = () => {

	if (performance.navigation.type == 1) {
		window.location.replace("#");

		if (typeof window.history.replaceState == 'function') {
			history.replaceState({}, '', window.location.href.slice(0, -1));
		}
	}

  return (
	  <div id="page_index_container">
	  <Header />
	  <Articles />
	  <Separator />
	  <About />
	  <Contact />
	  <Footer />
	  </div>
  );
};

export default Index
