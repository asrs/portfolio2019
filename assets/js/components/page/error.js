import React, {Component} from 'react';
import { withTranslation } from 'react-i18next';
import { faArrowLeft } from '@fortawesome/fontawesome-free-solid'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


class Articles extends Component {
	render() {

		const { t, i18n } = this.props;

		return (
			<div id="page_error_container">
				<header>
					<h1>{t('page_error')}</h1>
					<a href="/" title={t('back')} alt="Go back button">
						<FontAwesomeIcon className="icon-FontAwesome" icon={faArrowLeft} />
						{t('back')}
					</a>
				</header>
			</div>
		);
	}
}

export default withTranslation()(Articles);
