import React, {Component} from 'react';
import Axios from "axios";
import Cookies from "js-cookie";

import ArticlesSearch from './articles/articles_search.js';
import ArticlesTag from './articles/articles_tag.js';
import ArticlesContainer from './articles/articles_container.js';
import ArticlesMore from './articles/articles_more.js';

class Articles extends Component {

	constructor(props) {
		super(props);

		this.handleSearchChange = this.handleSearchChange.bind(this);
		this.handleSearchClick = this.handleSearchClick.bind(this);
		this.handleTag = this.handleTag.bind(this);
		this.handleMore = this.handleMore.bind(this);
		this.handleList = this.handleList.bind(this);

		this.state = {
			tag_value: "",
			search_value: "",
			articles_search: [],
			articles_list: {},
			articles_max: 0,
			articles_nb: 6
		};
	};

	componentWillMount() {
		Axios.get('/api/get_list')
			.then((response) => {
				return response.data;
			})
			.then ((data) => {
				this.setState({
					articles_list: data.data,
					articles_max: data.data.length
				});
			})
			.catch (error => console.log(error));
	}

	handleSearchChange = (e) => {
		let value = (e.target.value === "") ? "" : e.target.value.toLowerCase();

		this.handleList(this.state.tag_value, value);
	}

	handleSearchClick = () => {
		this.handleList(this.state.tag_value, "");
	};

	handleTag = (value) => {
		this.handleList(value.toLowerCase(), this.state.search_value);
	}

	handleMore = () => {
		this.setState({
			articles_nb: this.state.articles_nb + 3
		});
	};

	handleList = (tag_value, search_value) => {
		const list = this.state.articles_list;


		let ret = [];

		if (tag_value != "" && search_value != "") {
			ret = list.filter((e) => e.tag.toLowerCase() == tag_value)
			ret = ret.filter((e) => {
				if (Cookies.get("lng") == "fr")
					return e.title_fr.toLowerCase().includes(search_value);
				else
					return e.title_en.toLowerCase().includes(search_value);
			})
		}
		else if (tag_value != "" && search_value == "")
			ret = list.filter((e) => e.tag.toLowerCase() == tag_value);
		else if (tag_value == "" && search_value != "")
			ret = list.filter((e) => {
				if (Cookies.get("lng") == "fr")
					return e.title_fr.toLowerCase().includes(search_value);
				else
					return e.title_en.toLowerCase().includes(search_value);
			});

		this.setState({
			tag_value: tag_value,
			search_value: search_value,
			articles_search: ret
		});
	}

	render () {
		return (
			<section id="articles_section" className="top_section">

			<ArticlesSearch
			search_value={this.state.search_value}
			action_click={this.handleSearchClick}
			action_onchange={this.handleSearchChange}
			/>

			<ArticlesTag action={this.handleTag}/>

			<ArticlesContainer
			tag_value={this.state.tag_value}
			search_value={this.state.search_value}
			articles_list={this.state.articles_list}
			articles_search={this.state.articles_search}
			articles_nb={this.state.articles_nb}
			/>

			<ArticlesMore
			action={this.handleMore}
			articles_nb={this.state.articles_nb}
			articles_max={this.state.articles_max}
			search_value={this.state.search_value}
			tag_value={this.state.tag_value}
			/>
			</section>
		);
}
};

export default Articles;
