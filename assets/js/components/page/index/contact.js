import React, {Component} from 'react';
import { withTranslation } from 'react-i18next';
import ReCAPTCHA from "react-google-recaptcha";
import Axios from "axios";

class Contact extends Component {
	constructor(props) {
		super(props)

		this.state = {
			emailValue: "",
			messageValue: "",
			returnMail: "",
		}
		this.checkEmail = this.checkEmail.bind(this)
		this.checkMessage= this.checkMessage.bind(this)
		this.handleClickInput = this.handleClickInput.bind(this)
		this.handleClickTextarea = this.handleClickTextarea.bind(this)
		this.handleChangeEmail = this.handleChangeEmail.bind(this)
		this.handleChangeMessage = this.handleChangeMessage.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.handleMail= this.handleMail.bind(this)
	}

	checkEmail () {
		const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

		if (re.test(this.state.emailValue.toLowerCase())) {
			if ( ~contact_email_inp.className.indexOf(' emailError ')) 
				contact_email_inp.className = contact_email_inp.className.replace(' emailError ', '');
		}
		else {
			if ( ~contact_email_inp.className.indexOf(' emailError ') == false)
				contact_email_inp.className += " emailError ";
		}
		return (re.test(this.state.emailValue.toLowerCase()));
	}

	checkMessage () {
		const len = this.state.messageValue.length;

		if (len > 1) {
			if ( ~contact_message_inp.className.indexOf(' messageError ')) 
				contact_message_inp.className = contact_message_inp.className.replace(' messageError ', '');
		}
		else {
			if ( ~contact_message_inp.className.indexOf(' messageError ') == false)
				contact_message_inp.className += " messageError ";
		}
		return (len > 1)
	}

	handleClickInput () {
		contact_form_input.focus();
	}

	handleClickTextarea () {
		contact_form_textarea.focus();
	}

	handleChangeEmail (e) {

		const validity = this.checkEmail();

		this.setState({
			emailValue: e.target.value
		})
	}

	handleChangeMessage(e) {
		const validity = this.checkMessage();

		this.setState({
			messageValue: e.target.value
		})
	}

	handleSubmit(e) {
		e.preventDefault();
		const emailValidity = this.checkEmail()
		const messageValidity = this.checkMessage()

		if (emailValidity == true && messageValidity == true)
			this.handleMail(this.state.emailValue.toLowerCase(), this.state.messageValue)
	}
	
	handleMail (email, message) {
		//console.log("email: ", this.state.emailValue);
		//console.log("message: ", this.state.messageValue);
//		AXIOS POST SEND MAIL HERE
		Axios.post('/api/send_mail', {
			email: email,
			message: message
		})
		.then(response => { 
			contact_form_element.remove();
//			console.log(response)
			this.setState({
				returnMail: "true"
			})
		})
		.catch(error => {
//			console.log(error.response)
			contact_form_element.remove();
			this.setState({
				returnMail: "false"
			})
		});
	}

	render () {

		const { t, i18n } = this.props;

		const returnMailElementOk = (this.state.returnMail == "true") ? <h4> {t('mail_ok')} </h4> : null;
		const returnMailElementError = (this.state.returnMail == "false") ? <h4> {t('mail_error')} <br/> 'contact.mnhdrn@gmail.com' </h4> : null;

		return (
			<section id="contact_section" className="" >
			<div id="form_container">
				<form id="contact_form_element">
					<label id="contact_email_inp" htmlFor="email" className="inp">
						<input id="contact_form_input" type="email" name="email" placeholder=" " onChange={(e) => this.handleChangeEmail(e)}/>
						<span className="label" onClick={this.handleClickInput}>{t('placeholder_email')}</span>
						<span className="border"></span>
					</label>
					<label id="contact_message_inp" htmlFor="message" className="txtar">
						<textarea id="contact_form_textarea" name="message" className="text" placeholder=" " onChange={(e) => this.handleChangeMessage(e)}></textarea>
						<span className="label" onClick={this.handleClickTextarea}>{t('placeholder_message')}</span>
						<span className="border"></span>
					</label>
					<input className="form_submit" type="submit" name="submit" value={t('contact_submit')} onClick={(e) => this.handleSubmit(e)}/>
				</form>
				{returnMailElementOk}
				{returnMailElementError}
			</div>
			</section>
		);
	}
};
//<ReCAPTCHA className="recaptcha" theme="dark" sitekey="6LczcrUUAAAAANJ2wmA_I5-fXGWvN5hESYYgi0MF" />

export default withTranslation()(Contact);
