import React, {Component} from 'react';
import { withTranslation } from 'react-i18next';

class TechContainer extends Component {
	constructor(props) {
		super(props);

		this.handleHoverEnter = this.handleHoverEnter.bind(this)
		this.handleHoverLeave = this.handleHoverLeave.bind(this)
		this.state = {
			logoText: ""
		}
	}

	componentDidMount() {
		const elixirEl= document.getElementById('elixirLogo');
		const phoenixEl = document.getElementById('phoenixLogo');
		const reactEl = document.getElementById('reactLogo');
		const fedoraEl= document.getElementById('fedoraLogo');
		const rhelEl= document.getElementById('rhelLogo');
		const ansibleEl= document.getElementById('ansibleLogo');

		elixirEl.addEventListener("mouseenter", this.handleHoverEnter);
		elixirEl.addEventListener("mouseleave", this.handleHoverLeave);
		phoenixEl.addEventListener("mouseenter", this.handleHoverEnter);
		phoenixEl.addEventListener("mouseleave", this.handleHoverLeave);
		reactEl.addEventListener("mouseenter", this.handleHoverEnter);
		reactEl.addEventListener("mouseleave", this.handleHoverLeave);
		fedoraEl.addEventListener("mouseenter", this.handleHoverEnter);
		fedoraEl.addEventListener("mouseleave", this.handleHoverLeave);
		rhelEl.addEventListener("mouseenter", this.handleHoverEnter);
		rhelEl.addEventListener("mouseleave", this.handleHoverLeave);
		ansibleEl.addEventListener("mouseenter", this.handleHoverEnter);
		ansibleEl.addEventListener("mouseleave", this.handleHoverLeave);
	}

	componentWillUnmount() {
		const elixirEl= document.getElementById('elixirLogo');
		const phoenixEl = document.getElementById('phoenixLogo');
		const reactEl = document.getElementById('reactLogo');
		const fedoraEl= document.getElementById('fedoraLogo');
		const rhelEl= document.getElementById('rhelLogo');
		const ansibleEl= document.getElementById('ansibleLogo');

		elixirEl.removeEventListener("mouseenter", this.handleHoverEnter);
		elixirEl.removeEventListener("mouseleave", this.handleHoverLeave);
		phoenixEl.removeEventListener("mouseenter", this.handleHoverEnter);
		phoenixEl.removeEventListener("mouseleave", this.handleHoverLeave);
		reactEl.removeEventListener("mouseenter", this.handleHoverEnter);
		reactEl.removeEventListener("mouseleave", this.handleHoverLeave);
		fedoraEl.removeEventListener("mouseenter", this.handleHoverEnter);
		fedoraEl.removeEventListener("mouseleave", this.handleHoverLeave);
		rhelEl.removeEventListener("mouseenter", this.handleHoverEnter);
		rhelEl.removeEventListener("mouseleave", this.handleHoverLeave);
		ansibleEl.removeEventListener("mouseenter", this.handleHoverEnter);
		ansibleEl.removeEventListener("mouseleave", this.handleHoverLeave);
	}

	handleHoverEnter = (e) => {
		const detailEl = ' ' + tech_logo_detail.className + ' '

		if (e.path[0].id === "elixirLogo") {
			this.setState({
				logoText: 'elixir_description'
			});
		}
		else if (e.path[0].id === "phoenixLogo") {
			this.setState({
				logoText: 'phoenix_description'
			});
		}
		else if (e.path[0].id === "reactLogo") {
			this.setState({
				logoText: 'react_description'
			});
		}
		else if (e.path[0].id === "fedoraLogo") {
			this.setState({
				logoText: 'fedora_description'
			});
		}
		else if (e.path[0].id === "rhelLogo") {
			this.setState({
				logoText: 'rhel_description'
			});
		}
		else if (e.path[0].id === "ansibleLogo") {
			this.setState({
				logoText: 'ansible_description'
			});
		}

		if ( ~detailEl.indexOf(' is-active ') ) {
			tech_logo_detail.className = detailEl.replace(' is-active ', '');
		} else {
			tech_logo_detail.className += 'is-active';
		};
	}

	handleHoverLeave= (e) => {
		const detailEl = ' ' + tech_logo_detail.className + ' '

		if ( ~detailEl.indexOf(' is-active ') ) {
			tech_logo_detail.className = detailEl.replace(' is-active ', '');
		} else {
			tech_logo_detail.className += 'is-active';
		};
	}
	render () {

		const { t, i18n } = this.props;

		return (
			<div id="tech_container">
				<div id="tech_logo_pres">
					<span id="elixirLogo" ><img src="/images/elixir_logo.png" alt="Elixir Logo"/></span>
					<span id="phoenixLogo" ><img src="/images/phoenix_logo.png" alt="Phoenix Logo"/></span>
					<span id="reactLogo" ><img src="/images/react_logo.png" alt="React Logo"/></span>
					<span id="rhelLogo" ><img src="/images/rhel_logo.svg" alt="rhel Logo"/></span>
					<span id="fedoraLogo" ><img src="/images/fedora_logo.png" alt="Fedora Logo"/></span>
					<span id="ansibleLogo" ><img src="/images/ansible_logo.png" alt="Ansible Logo"/></span>
				</div>
				<div id="tech_logo_detail" className="" >
					<p>{t(this.state.logoText)}</p>
				</div>
			</div>
		);
	}
};

export default withTranslation()(TechContainer);

