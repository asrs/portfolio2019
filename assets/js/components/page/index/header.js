import React, {Component} from 'react';
import { withTranslation } from 'react-i18next';

class Header extends Component {
	render () {

		const { t, i18n } = this.props;

		let parentEl = document.getElementById('page_index_header_text');
		let text = t('header_text_top');

		const wrappedParts = text.split('').map((stringPart, index) => {
			return (
				index === 0 ? null : <span className='letter' key={index + 8}>{stringPart}</span>
			)
		})

		return (
		<header>
			<div id="page_index_header_img"></div>
			<div id="page_index_header_text_top" className=".wrapper">
			{wrappedParts}
			</div>
			<div id="page_index_header_text_bottom">
			<span>{'{ '}</span>
			<span className="growing">{t('header_text_bottom')}</span>
			<span>{' }'}</span>
			</div>
			<a href="#articles_section" title={t('begin')} alt={t('begin')} ><button>{t('begin')}</button></a>
			<div id="unsplash_credit">{t('unsplash_credit')}<a href="https://unsplash.com" title="Unsplash.com" alt="Unsplash.com" >Unsplash</a></div>
		</header>
		);
	}
};

export default withTranslation()(Header);
